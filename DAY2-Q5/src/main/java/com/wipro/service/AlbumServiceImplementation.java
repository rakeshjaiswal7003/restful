package com.wipro.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.model.Album;
import com.wipro.repository.AlbumRepo;
@Service
public class AlbumServiceImplementation implements AlbumService
{
	@Autowired
	AlbumRepo ar;
	@Override
	public Album saveAlbum(Album a) {
		// TODO Auto-generated method stub
		return ar.save(a);
	}

	@Override
	public Optional<Album> findById(String i) {
		// TODO Auto-generated method stub
		return ar.findById(i);
	}
	

}
