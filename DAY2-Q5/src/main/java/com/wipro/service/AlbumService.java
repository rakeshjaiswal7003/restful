package com.wipro.service;

import java.util.Optional;

import com.wipro.model.Album;

public interface AlbumService {
	Album saveAlbum(Album a);
	Optional<Album>	findById(String i);

}
