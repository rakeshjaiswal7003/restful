package com.wipro.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.model.Album;
import com.wipro.service.AlbumServiceImplementation;

@RestController
public class AlbumController {
	@Autowired
	AlbumServiceImplementation asi;
	@PostMapping("addalbum")
	public Album addAlbum(Album a)
	{
		return asi.saveAlbum(a);
		
	}
	@GetMapping("findalbum")
	public Optional<Album> findAlbum(@RequestParam("title") String ti)
	{
		return asi.findById(ti);
		
	}

}
