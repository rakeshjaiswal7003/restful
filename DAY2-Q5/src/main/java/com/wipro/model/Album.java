package com.wipro.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Album {
	@Id
	private String title;
	private String singer;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSinger() {
		return singer;
	}
	public void setSinger(String singer) {
		this.singer = singer;
	}
	
	
}
