package com.wipro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.model.Album;

public interface AlbumRepo extends JpaRepository<Album, String>
{
	

}
