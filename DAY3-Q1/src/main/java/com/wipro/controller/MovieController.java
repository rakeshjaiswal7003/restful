package com.wipro.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.model.Movie;
import com.wipro.service.MovieServiceImplementation;

@RestController
public class MovieController {
	@Autowired
MovieServiceImplementation msi;
	@Autowired
	MovieController mc;
	
	public void addindPreStoredValues()
	{
		List<Movie> l=new ArrayList<Movie>();
		l.add(new Movie("M1","Top Gun","Tom Crusie",350000));
		l.add(new Movie("M2","Race Guram","Arjuna",8000));
		l.add(new Movie("M3","Wonder","TWons",3000));
		l.add(new Movie("M4","Beautiful","Ellen",350000));
		l.add(new Movie("M5","Genine","Roso",350000));
		l.add(new Movie("M6","World","Bill",350000));
		l.add(new Movie("M7","Her","Wanda",350000));
		l.add(new Movie("M8","Mike","Wang hu",350000));
		l.add(new Movie("M9","Magic","Halie",350000));
		l.add(new Movie("M10","Horrors","Bettiee",350000));
		msi.saveAllMovie(l);
	}

	@GetMapping(value="gettextxml",produces=MediaType.TEXT_XML_VALUE)
	public List<Movie>getTextXml()
	{
		mc.addindPreStoredValues();
		return msi.findAllMovie();
		
	}
	
	@GetMapping(value="getapplicationxml",produces=MediaType.APPLICATION_XML_VALUE)
	public List<Movie> getApplicationXml()
	{
		mc.addindPreStoredValues();
		return msi.findAllMovie();
		
	}
	
	@GetMapping(value="getapplicationjson",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Movie> getApplicatioJson()
	{
		mc.addindPreStoredValues();
		return msi.findAllMovie();
		
	}
	
	@GetMapping(value="getmovie/{movieId}",produces=MediaType.APPLICATION_XML_VALUE)
	public Optional<Movie> getMovieById(@PathVariable("movieId") String id)
	{
		mc.addindPreStoredValues();
		return msi.findByMid(id);
		
	}
	

}
