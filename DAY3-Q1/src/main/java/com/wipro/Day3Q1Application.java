package com.wipro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day3Q1Application {

	public static void main(String[] args) {
		SpringApplication.run(Day3Q1Application.class, args);
	}

}
