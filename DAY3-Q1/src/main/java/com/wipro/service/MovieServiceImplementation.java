package com.wipro.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.model.Movie;
import com.wipro.repository.MovieRepo;
@Service
public class MovieServiceImplementation implements MovieService
{
@Autowired
MovieRepo mr;
	@Override
	public List<Movie> findAllMovie() {
		// TODO Auto-generated method stub
		
		return mr.findAll();
	}

	@Override
	public Optional<Movie> findByMid(String i) {
		// TODO Auto-generated method stub
		return  mr.findById(i);
	}

	@Override
	public void saveAllMovie(List<Movie> l) {
		// TODO Auto-generated method stub
		mr.saveAll(l);
		
	}

}
