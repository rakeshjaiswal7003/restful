package com.wipro.service;

import java.util.List;
import java.util.Optional;

import com.wipro.model.Movie;

public interface MovieService {
	List<Movie> findAllMovie();
	Optional<Movie> findByMid(String i);
	void saveAllMovie(List<Movie> l);

}
