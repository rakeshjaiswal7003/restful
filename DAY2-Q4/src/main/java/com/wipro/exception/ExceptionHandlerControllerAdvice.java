package com.wipro.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionHandlerControllerAdvice {
	@ExceptionHandler(Exception.class)
	@ResponseBody
	String handleException(Exception e)
	{
		return e.getMessage();
		
	}

}
