package com.wipro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.dao.AccountDao;
import com.wipro.model.Account;
@Service
public class AccountServiceImplementaion implements AccountService
{	@Autowired
	AccountDao ad;
	@Override
	public Account openAccount(Account a) {
		// TODO Auto-generated method stub
		return ad.save(a);
	}
	@Override
	public Account findAccount(int i) {
		// TODO Auto-generated method stub
		return ad.findByAid(i);
	}

}
