package com.wipro.service;

import com.wipro.model.Account;

public interface AccountService {
	Account openAccount(Account a);
	Account findAccount(int i);
}
