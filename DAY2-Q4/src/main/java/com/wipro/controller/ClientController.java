package com.wipro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ClientController {
	@RequestMapping(" ")
	public String home()
	{
		return "Client";
		
	}
	
	@RequestMapping("opena")
	public String openacc()
	{
		return "Opena";
		
	}
	
	@RequestMapping("deposit")
	public String deposit()
	{
		return "Deposit";
		
	}
	
	@RequestMapping("withdraw")
	public String withdraw()
	{
		return "Withdraw";
		
	}
	
	@RequestMapping("balance")
	public String balance()
	{
		return "Balance";
		
	}

}
