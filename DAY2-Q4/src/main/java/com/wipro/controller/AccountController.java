package com.wipro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.model.Account;
import com.wipro.service.AccountServiceImplementaion;

@RestController
public class AccountController {
	@Autowired
	AccountServiceImplementaion asi;
	@PostMapping("openaccount")
	public Account openAccount(Account a)
	{
		return asi.openAccount(a);
		
	}
	//depositmoney withdrawmoney checkbalance
	@PostMapping("depositmoney")
	public Account depositMoney(@RequestParam("aid") int id,@RequestParam("bal") int bal) throws Exception
	{
		Account a=asi.findAccount(id);
		if(a!=null)
		a.setBal(a.getBal()+bal);
		else
			throw new Exception("Account number not found");
		return asi.openAccount(a);
		
	}
	@PostMapping("withdrawmoney")
	public Account withdrawMoney(Account a) throws Exception
	{
		Account b=asi.findAccount(a.getAid());
		if(b!=null && b.getAname().equalsIgnoreCase(a.getAname()))
		{
			if(b.getBal()>a.getBal())
				b.setBal(b.getBal()-a.getBal());
			else
				throw new Exception("Not sufficent balance for the withdraw amount");
		}
		else
			throw new Exception("Account number not found in db or account holder name incorrect");
		return asi.openAccount(b);
		
	}
	
	@PostMapping("checkbalance")
	public int depositMoney(@RequestParam("aid") int id) throws Exception
	{
		Account a=asi.findAccount(id);
		if(a!=null)
		return a.getBal();
		else
			throw new Exception("Incorrect account number");
		
	}
	
}
