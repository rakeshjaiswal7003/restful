package com.wipro.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.model.Account;

public interface AccountDao extends JpaRepository<Account, Integer>
{
Account findByAid(int i);
}
