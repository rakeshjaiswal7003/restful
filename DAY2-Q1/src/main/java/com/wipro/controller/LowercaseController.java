package com.wipro.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LowercaseController {
	@GetMapping("lower")
	public String doLower(@RequestParam("name") String n)
	{
		
		return n.toLowerCase();
		
	}

}
