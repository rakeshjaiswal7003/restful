package com.wipro.controller;

import java.time.LocalDate;
import java.time.Period;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AgeController {
	@GetMapping("currentage")
	public int age(@RequestParam("bd") String n1, @RequestParam("ad") String n2)
	{	String s[]=n1.split("-");
		int y=Integer.parseInt(s[0]);
		int m=Integer.parseInt(s[1]);
		int d=Integer.parseInt(s[2]);
		     
		LocalDate birthday = LocalDate.of(y,m, d);
		String s1[]=n2.split("-");
		 y=Integer.parseInt(s1[0]);
		 m=Integer.parseInt(s1[1]);
		 d=Integer.parseInt(s1[2]);
		 LocalDate anydate = LocalDate.of(y,m, d);
		Period p = Period.between(birthday, anydate);
		return p.getYears();
		
	}

}
