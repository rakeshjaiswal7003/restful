package com.wipro.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.model.Employee;

public interface EmployeeDao extends JpaRepository<Employee, Integer>{

}
