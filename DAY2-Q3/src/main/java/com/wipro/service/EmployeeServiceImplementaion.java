package com.wipro.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.dao.EmployeeDao;
import com.wipro.model.Employee;
@Service
public class EmployeeServiceImplementaion implements EmployeeService
{	@Autowired
	EmployeeDao ed;
	@Override
	public Employee addEmployee(Employee e) {
		// TODO Auto-generated method stub
		return ed.save(e);
	}

	@Override
	public Employee updateEmployee(Employee e) {
		// TODO Auto-generated method stub
		return ed.save(e);
	}

	@Override
	public List<Employee> findAllEmployee() {
		// TODO Auto-generated method stub
		return ed.findAll();
	}

	@Override
	public Optional<Employee> findEById(int i) {
		// TODO Auto-generated method stub
		return ed.findById(i);
	}

	@Override
	public void deleteByEid(int i) {
		// TODO Auto-generated method stub
		ed.deleteById(i);
	}
	

}
