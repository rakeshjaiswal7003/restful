package com.wipro.service;

import java.util.List;
import java.util.Optional;

import com.wipro.model.Employee;

public interface EmployeeService {
	Employee addEmployee(Employee e);
	Employee updateEmployee(Employee e);
	List<Employee> findAllEmployee();
	Optional<Employee> findEById(int i);
	void deleteByEid(int i);
}
