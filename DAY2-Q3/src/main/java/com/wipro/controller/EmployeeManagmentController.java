package com.wipro.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.model.Employee;
import com.wipro.service.EmployeeServiceImplementaion;

@RestController
public class EmployeeManagmentController {
	@Autowired
	EmployeeServiceImplementaion esi;
	
	@PostMapping("savee")
	public Employee saveE(Employee e)
	{	
		return esi.addEmployee(e);
		
	}
	
	@GetMapping("findemployee")
	public Optional<Employee> findEmployee(@RequestParam("eid") int id)
	{
		return esi.findEById(id);
		
	}
	
	@GetMapping("showalle")
	public List<Employee> showallE()
	{
		return esi.findAllEmployee();
		
	}
	
	@PostMapping("updateemployee")
	public Employee updateEmployee(Employee e)
	{
		return esi.updateEmployee(e);
	}
	
	@PostMapping("deleteemployee")
	public String deleteE(@RequestParam("eid") int eid)
	{ // could have used controller advice(Used in DAY2-Q4) but just used try catch generally as it was
		//not mentioned to handle exceptions
		try {
		esi.deleteByEid(eid);
		return "deleted";
		}
		catch(Exception e)
		{
			return e.getMessage();
		}
		
		
	}

}
