package com.wipro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ClientController {
	@RequestMapping("/")
	public String home()
	{
		return "Client";
	}
	@RequestMapping("inserte")
	public String insertE()
	{
		return "Eform";
		
	}
	@RequestMapping("finde")
	public String findE()
	{
		return "Finde";
		
	}
	
	@RequestMapping("updatee")
	public String updateE()
	{
		return "Updatee";
		
	}
	
	@RequestMapping("deletee")
	public String deleteE()
	{
		return "Deletee";
		
	}
	
	
	
}
