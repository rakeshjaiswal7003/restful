package com.wipro.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class DoubleValueController {
	@GetMapping("doubleit")
	public int doubleIt(@RequestParam("t") int n)
	{
		return n*2;
		
	}
	
	

}
