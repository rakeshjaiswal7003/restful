package com.wipro.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CardController {
	
	@GetMapping("approval/{cno}")
	public String validate(@PathVariable("cno") Integer cno)
	{
		if((cno%10)%2==0)
			return "true";
		else
			return "false";
		
	}

}
