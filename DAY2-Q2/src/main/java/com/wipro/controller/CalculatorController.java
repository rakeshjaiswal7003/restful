package com.wipro.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {
	@GetMapping("calculate")
	public int calculate(@RequestParam("t1") int n1,@RequestParam("t2") int n2,@RequestParam("but") String bvalue)
	{
		if(bvalue.equals("ADD"))
		return add(n1,n2);
		else
			return subtract(n1, n2);
		
	}
	
	public int add(int n1,int n2)
	{
		return n1+n2;
		
	}
	
	public int subtract(int n1,int n2)
	{
		return n1-n2;
		
	}
}
