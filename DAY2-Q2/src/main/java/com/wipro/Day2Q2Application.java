package com.wipro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day2Q2Application {

	public static void main(String[] args) {
		SpringApplication.run(Day2Q2Application.class, args);
	}

}
